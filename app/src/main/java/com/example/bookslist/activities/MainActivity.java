package com.example.bookslist.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.bookslist.R;
import com.example.bookslist.adapters.BooksAdapter;
import com.example.bookslist.database.dao.BooksDAO;
import com.example.bookslist.database.dto.BookDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.bookslist.database.ws.RetrofitSingleton;
import com.example.bookslist.database.ws.WSInterface;
import com.example.bookslist.models.Book;
import com.example.bookslist.models.BookApi;
import com.example.bookslist.models.BookApiResponse;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private BooksAdapter booksAdapter;
	private RecyclerView recyclerView;
    Context context;
	private List<BookApi> newBooksApiList = new ArrayList<>();
    private List<BookDTO> newBooksDTOList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this.getApplicationContext();
        BooksDAO.clearDatabase(this);

        getBooks();
    }

    private void getBooks() {
        WSInterface service = RetrofitSingleton.getInstance().retrofit.create(WSInterface.class);
        Call<BookApiResponse> callAsync = service.getBooks(true);
        callAsync.enqueue(new Callback<BookApiResponse>()
        {
            @Override
            public void onResponse(Call<BookApiResponse> call, Response<BookApiResponse> response) {
                Log.w(TAG, response.toString());
                if (response.isSuccessful())
                {
                    BookApiResponse responseApi = response.body();
                    Log.w(TAG, responseApi.toString());
                    List<BookApi> bookApiList = responseApi.data;
                    Log.w(TAG, "API");
                    Log.w(TAG, bookApiList.toString());

                    List<BookDTO> booksDTOList = BooksDAO.booksApiToBooksDTO(bookApiList);
                    booksDTOList.forEach(bookDTO -> {
                        Log.w(TAG,bookDTO.toString());
                        // newBooksDTOList.add(bookDTO);
                        BooksDAO.ajouterBook(bookDTO, context);
                    });

                    // liste :
                    recyclerView = findViewById(R.id.list_books);

                    // à ajouter pour de meilleures performances :
                    recyclerView.setHasFixedSize(true);

                    // layout manager, décrivant comment les items sont disposés :
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    recyclerView.setLayoutManager(layoutManager);

                    // liste d'exemple :
                    List<BookDTO> listBooks = BooksDAO.getBooksList(context);
                    Log.w(TAG, listBooks.toString());

                    // adapter :
                    booksAdapter = new BooksAdapter(listBooks);
                    recyclerView.setAdapter(booksAdapter);

                    booksAdapter.notifyDataSetChanged();
                } else {
                    Log.w(TAG, "API fail");
                }
            }

            @Override
            public void onFailure(Call<BookApiResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}