package com.example.bookslist.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentTransaction;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.example.bookslist.R;
import com.example.bookslist.database.bdd.BaseContrat;
import com.example.bookslist.database.bdd.DatabaseHelper;
import com.example.bookslist.database.dao.BooksDAO;
import com.example.bookslist.database.dto.BookDTO;
import com.example.bookslist.databinding.ActivityDetailBinding;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "0";
    private static final String TAG = "DetailActivity";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        // argument :
        String id = getIntent().getStringExtra(EXTRA_ID);
        Log.i(TAG, String.valueOf(id));

        BookDTO book = BooksDAO.getBook(Integer.parseInt(id), this.getApplicationContext());
        Log.i(TAG, book.toString());

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            //Setting a dynamic title at runtime. Here, it displays the current time.
            actionBar.setTitle(book.title);
        }

        context = this.getApplicationContext();

        ActivityDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setBook(book);
        ImageView imageView = findViewById(R.id.pictureImage);
        Glide.with(this).load(book.getPicture()).into(imageView);

        ToggleButton toggleFavorite = (ToggleButton) findViewById(R.id.toggleFavorite);
        toggleFavorite.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                // récupération de la base de données :
                DatabaseHelper databaseHelper = new DatabaseHelper(context);
                SQLiteDatabase db = databaseHelper.getWritableDatabase();
                String toggleFavorite = book.getIsFavorite();
                Log.w(TAG,toggleFavorite);
                Boolean toggleFavoriteBool = !Boolean.valueOf(toggleFavorite);
                Log.w(TAG,toggleFavoriteBool.toString());
                toggleFavorite = String.valueOf(toggleFavoriteBool);
                Log.w(TAG,toggleFavorite);

                ContentValues cv = new ContentValues();
                cv.put(BaseContrat.BooksContrat.COLONNE_FAVORITE, toggleFavorite);
                db.update(BaseContrat.BooksContrat.TABLE_BOOKS, cv, "_id = ?", new String[]{id});
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}