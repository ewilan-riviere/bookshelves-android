package com.example.bookslist.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bookslist.R;
import com.example.bookslist.activities.DetailActivity;
import com.example.bookslist.activities.MainActivity;
// import com.example.bookslist.fragments.DetailFragment;
import com.example.bookslist.database.dto.BookDTO;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BookViewHolder>
{
	
	// Liste de books :
	private List<BookDTO> listeBooks;
	private static Context context;
	private static final String TAG = "BooksAdapter";
	
	public BooksAdapter(List<BookDTO> listeBooks)
	{
		this.listeBooks = listeBooks;
	}
	
	@NonNull
	@Override
	public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		View viewBook = LayoutInflater
				.from(parent.getContext())
				.inflate(R.layout.item_book, parent, false);
		return new BookViewHolder(viewBook);
	}
	
	@Override
	public void onBindViewHolder(@NonNull BookViewHolder holder, int position)
	{
		holder.id.setId((int) listeBooks.get(position).getId());
		holder.title.setText(listeBooks.get(position).getTitle());
		holder.serie.setText(listeBooks.get(position).getSerie() + ", vol." + listeBooks.get(position).getVolume());
		holder.author.setText(listeBooks.get(position).getAuthor());
		Uri picture = Uri.parse(listeBooks.get(position).getPicture());
		holder.picture.setImageURI(picture);

		Glide.with(holder.itemView.getContext())
				.load(picture)
				.into(holder.picture);

		holder.summary.setText(listeBooks.get(position).getSummary());

	}

	@Override
	public int getItemCount()
	{
		return listeBooks.size();
	}
	
	public void updateBooks(List<BookDTO> listeBooks)
	{
		this.listeBooks = listeBooks;
		notifyDataSetChanged();
	}
	
	// Viewholder :
	class BookViewHolder extends RecyclerView.ViewHolder
	{
		public MaterialCardView id;
		public TextView title;
		public TextView serie;
		public TextView author;
		public ImageView picture;
		public TextView summary;

		public BookViewHolder(@NonNull View itemView)
		{
			super(itemView);
			id = itemView.findViewById(R.id.id);
			title = itemView.findViewById(R.id.title);
			serie = itemView.findViewById(R.id.serie);
			author = itemView.findViewById(R.id.author);
			picture = itemView.findViewById(R.id.picture);
			summary = itemView.findViewById(R.id.summary);
			
			// listener clic :
			itemView.setOnClickListener(v -> {
				
				// récupérer book cliquée :
				BookDTO bookDTO = listeBooks.get(getAdapterPosition());
				Log.i(TAG, "CLick on item from view!");
				Log.i(TAG, bookDTO.toString());
				
				// if (mainActivity.findViewById(R.id.conteneur_detail) != null)
				// {
				// 	// afficher détail "tablette" :
				// 	DetailFragment fragment = new DetailFragment();
				// 	Bundle bundle = new Bundle();
				// 	bundle.putString(DetailFragment.EXTRA_TITLE, bookDTO.title);
				// 	fragment.setArguments(bundle);
					
				// 	// ajout :
				// 	FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
				// 	transaction.replace(R.id.conteneur_detail, fragment, "tagdetail");
				// 	transaction.commit();
				// }
				// else
				// {
				// 	// afficher détail "smartphone" :
				Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
				intent.putExtra(DetailActivity.EXTRA_ID, String.valueOf(bookDTO.id));
				itemView.getContext().startActivity(intent);
				// }
			});
		}
	}

}
