package com.example.bookslist.database.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper
{
	
	public DatabaseHelper(@Nullable Context context)
	{
		super(context, "books.db", null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL("CREATE TABLE " + BaseContrat.BooksContrat.TABLE_BOOKS + " ("
				+ BaseContrat.BooksContrat._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ BaseContrat.BooksContrat.COLONNE_TITLE + " TEXT NOT NULL, "
				+ BaseContrat.BooksContrat.COLONNE_AUTHOR + " TEXT NOT NULL, "
				+ BaseContrat.BooksContrat.COLONNE_VOLUME + " INTEGER, "
				+ BaseContrat.BooksContrat.COLONNE_SERIE + " TEXT, "
				+ BaseContrat.BooksContrat.COLONNE_PICTURE + " TEXT,"
				+ BaseContrat.BooksContrat.COLONNE_SUMMARY + " TEXT,"
				+ BaseContrat.BooksContrat.COLONNE_LANG + " TEXT,"
				+ BaseContrat.BooksContrat.COLONNE_PUBLISHER + " TEXT,"
				+ BaseContrat.BooksContrat.COLONNE_FAVORITE + " TEXT"
				+ ")");

	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS books");

		// Create tables again
		onCreate(db);
	}
}
