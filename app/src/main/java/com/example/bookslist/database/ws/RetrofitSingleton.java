package com.example.bookslist.database.ws;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingleton
{
	
	private static final RetrofitSingleton instance = new RetrofitSingleton();
	
	public Retrofit retrofit;
	
	// Getter singleton :
	public static RetrofitSingleton getInstance()
	{
		return instance;
	}
	
	// Constructeur privé :
	private RetrofitSingleton()
	{
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();// add your other interceptors …// add logging as last interceptor
		httpClient.addInterceptor(logging);  // <-- this is the important line!Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build();

		retrofit = new Retrofit.Builder()
				.client(httpClient.build())
				.baseUrl("https://vivacia.bookshelves.ink/")
				.addConverterFactory(GsonConverterFactory.create())
				.build();
	}
	
}
