package com.example.bookslist.database.ws;

import com.example.bookslist.models.BookApi;
import com.example.bookslist.models.BookApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WSInterface
{
	
	// appel get :
	@GET("/api/books")
	Call<BookApiResponse> getBooks(@Query("mobile")  boolean mobile);
	
}
