package com.example.bookslist.database.bdd;

import android.provider.BaseColumns;

public final class BaseContrat
{
	
	private BaseContrat() {}
	
	public static class BooksContrat implements BaseColumns
	{
		public static final String TABLE_BOOKS = "books";
		public static final String COLONNE_TITLE = "title";
		public static final String COLONNE_AUTHOR = "author";
		public static final String COLONNE_VOLUME = "volume";
		public static final String COLONNE_SERIE = "serie";
		public static final String COLONNE_PICTURE ="picture";
		public static final String COLONNE_SUMMARY ="summary";
		public static final String COLONNE_LANG ="lang";
		public static final String COLONNE_PUBLISHER ="publisher";
		public static final String COLONNE_FAVORITE ="favorite";

	}
	
}
