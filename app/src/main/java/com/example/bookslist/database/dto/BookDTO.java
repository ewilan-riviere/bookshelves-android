package com.example.bookslist.database.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;

@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class BookDTO
{
	@NonNull public long id;
	@NonNull public String title;
	@NonNull public String author;
	private int volume;
	private String serie;
	private String picture;
	private String summary;
	private String language;
	private String publisher;
	private String isFavorite;

}
