package com.example.bookslist.database.ws;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

public class ReseauHelper
{
	
	public static boolean estConnecte(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
			if (networkCapabilities != null)
			{
				return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
						|| networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
						|| networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN);
			}
		}
		else
		{
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if (networkInfo != null)
			{
				return networkInfo.getType() == ConnectivityManager.TYPE_WIFI
						|| networkInfo.getType() == ConnectivityManager.TYPE_MOBILE
						|| networkInfo.getType() == ConnectivityManager.TYPE_VPN;
			}
		}
		return false;
	}
	
}
