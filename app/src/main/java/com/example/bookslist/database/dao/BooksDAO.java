package com.example.bookslist.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.bookslist.database.bdd.BaseContrat;
import com.example.bookslist.database.bdd.DatabaseHelper;
import com.example.bookslist.database.dto.BookDTO;
import com.example.bookslist.models.Book;
import com.example.bookslist.models.BookApi;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BooksDAO
{
	private static final String TAG = "BooksDAO";

	public static List<BookDTO> getBooksList(Context context)
	{
		// récupération de la base de données :
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		// tri :
		String tri = BaseContrat.BooksContrat.COLONNE_SERIE + " ASC, " + BaseContrat.BooksContrat.COLONNE_VOLUME + " ASC" ;

		// requête :
		Cursor cursor = db.query(
				false, // true si SELECT DISTINCT
				BaseContrat.BooksContrat.TABLE_BOOKS, // table sur laquelle faire la requète
				null, // colonnes à retourner
				null, // colonnes pour la clause WHERE
				null, // valeurs pour la clause WHERE
				null, // GROUP BY (inactif)
				null, // HAVING (inactif)
				tri, // ordre de tri
				null); // LIMIT (inactif)
		
		List<BookDTO> booksList = new ArrayList<>();
		if (cursor != null)
		{
			try
			{
				while (cursor.moveToNext())
				{
					// conversion des données remontées en un objet métier :
					booksList.add(new BookDTO(
							cursor.getLong(cursor.getColumnIndex(BaseContrat.BooksContrat._ID)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_TITLE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_AUTHOR)),
							cursor.getInt(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_VOLUME)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_SERIE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_PICTURE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_SUMMARY)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_LANG)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_PUBLISHER)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_FAVORITE))
					));
				}
			}
			catch (Exception exception)
			{
				exception.printStackTrace();
			}
			finally
			{
				cursor.close();
			}
		}
		return booksList;
	}

	public static BookDTO getBook(int id, Context context)
	{
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();

		Cursor cursor =  db.rawQuery("select * from " + BaseContrat.BooksContrat.TABLE_BOOKS + " where " + BaseContrat.BooksContrat._ID + "='" + id + "'" , null);
		BookDTO book = new BookDTO();
		if (cursor != null) {
			try
			{
				while (cursor.moveToNext())
				{
					// conversion des données remontées en un objet métier :
					book = new BookDTO(
							cursor.getLong(cursor.getColumnIndex(BaseContrat.BooksContrat._ID)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_TITLE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_AUTHOR)),
							cursor.getInt(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_VOLUME)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_SERIE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_PICTURE)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_SUMMARY)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_LANG)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_PUBLISHER)),
							cursor.getString(cursor.getColumnIndex(BaseContrat.BooksContrat.COLONNE_FAVORITE))
					);
				}
			}
			catch (Exception exception)
			{
				exception.printStackTrace();
			}
			finally
			{
				cursor.close();
			}

		}


		return book;
	}

	public static void clearDatabase(Context context) {
		context.deleteDatabase("books.db");
	}

	public static long ajouterBook(BookDTO bookDTO, Context context)
	{
		// récupération de la base de données :
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		
		// objet de valeurs :
		ContentValues values = new ContentValues();
		values.put(BaseContrat.BooksContrat.COLONNE_TITLE, bookDTO.getTitle());
		values.put(BaseContrat.BooksContrat.COLONNE_AUTHOR, bookDTO.getAuthor());
		values.put(BaseContrat.BooksContrat.COLONNE_VOLUME, bookDTO.getVolume());
		values.put(BaseContrat.BooksContrat.COLONNE_SERIE, bookDTO.getSerie());
		values.put(BaseContrat.BooksContrat.COLONNE_PICTURE, bookDTO.getPicture());
		values.put(BaseContrat.BooksContrat.COLONNE_SUMMARY, bookDTO.getSummary());
		values.put(BaseContrat.BooksContrat.COLONNE_LANG, bookDTO.getLanguage());
		values.put(BaseContrat.BooksContrat.COLONNE_PUBLISHER, bookDTO.getPublisher());
		values.put(BaseContrat.BooksContrat.COLONNE_FAVORITE, bookDTO.getIsFavorite());

		// ajout :
		return db.insert(BaseContrat.BooksContrat.TABLE_BOOKS, null, values);
	}

	public static List<Book> booksSeeds(Context context) {
        List<Book> listBooks = new ArrayList<>();
        Book aventuriersMer01 = new Book(
        		1,
                "Le vaisseau magique",
                "Robin Hobb",
                "9782756406374",
                5.0,
                1,
                "Les Aventuriers de la mer",
                LocalDate.of(2012,01,8),
                "https://vivacia.bookshelves.ink/storage/media/books/805/conversions/le-vaisseau-magique-thumbnail.webp"
        );
        Book pacte01 = new Book(
        		2,
                "Ellana",
                "Pierre Bottero",
                "9782700239935",
                6.0,
                1,
                "Le Pacte des Marchombres",
                LocalDate.of(2016,05,17),
                "https://vivacia.bookshelves.ink/storage/media/books/664/conversions/ellana-thumbnail.webp"
        );
        Book enfantTerre01 = new Book(
        		3,
                "Le clan de l'ours des cavernes",
                "Jean M. Auel",
                "9782266122122",
                5.0,
                1,
                "Les Enfants de la Terre",
                LocalDate.of(1980,01,13),
                "https://vivacia.bookshelves.ink/storage/media/books/606/conversions/le-clan-de-lours-des-cavernes-thumbnail.webp"
        );
		Book enfantTerre02 = new Book(
				4,
				"La Vallée des chevaux",
				"Jean M. Auel",
				"9782266122122",
				5.0,
				2,
				"Les Enfants de la Terre",
				LocalDate.of(1980,01,13),
				"https://vivacia.bookshelves.ink/storage/media/books/607/conversions/la-vallee-des-chevaux-thumbnail.webp"
		);
		Book enfantTerre03 = new Book(
				5,
				"Les chasseurs de mammouths",
				"Jean M. Auel",
				"9782266122122",
				5.0,
				3,
				"Les Enfants de la Terre",
				LocalDate.of(1980,01,13),
				"https://vivacia.bookshelves.ink/storage/media/books/608/conversions/les-chasseurs-de-mammouths-thumbnail.webp"
		);
		Book enfantTerre04 = new Book(
				6,
				"Le grand voyage",
				"Jean M. Auel",
				"9782266122122",
				5.0,
				4,
				"Les Enfants de la Terre",
				LocalDate.of(1980,01,13),
				"https://vivacia.bookshelves.ink/storage/media/books/609/conversions/le-grand-voyage-thumbnail.webp"
		);
		Book enfantTerre05 = new Book(
				7,
				"Les refuges de pierre",
				"Jean M. Auel",
				"9782266122122",
				5.0,
				5,
				"Les Enfants de la Terre",
				LocalDate.of(1980,01,13),
				"https://vivacia.bookshelves.ink/storage/media/books/610/conversions/les-refuges-de-pierre-thumbnail.webp"
		);
		Book enfantTerre06 = new Book(
				8,
				"Le pays des grottes sacrées",
				"Jean M. Auel",
				"9782266122122",
				5.0,
				6,
				"Les Enfants de la Terre",
				LocalDate.of(1980,01,13),
				"https://vivacia.bookshelves.ink/storage/media/books/611/conversions/le-pays-des-grottes-sacrees-thumbnail.webp"
		);
		Book aventuriersMer02 = new Book(
        		9,
                "Le navire aux esclaves",
                "Robin Hobb",
                "9782756406374",
                5.0,
                1,
                "Les Aventuriers de la mer",
                LocalDate.of(2012,01,8),
                "https://vivacia.bookshelves.ink/storage/media/books/806/conversions/le-navire-aux-esclaves-thumbnail.webp"
        );
		Book aventuriersMer03 = new Book(
        		10,
                "La conquête de la liberté",
                "Robin Hobb",
                "9782756406374",
                5.0,
                1,
                "Les Aventuriers de la mer",
                LocalDate.of(2012,01,8),
                "https://vivacia.bookshelves.ink/storage/media/books/807/conversions/la-conquete-de-la-liberte-thumbnail.webp"
        );
        listBooks.add(aventuriersMer01);
        listBooks.add(pacte01);
        listBooks.add(enfantTerre01);
		listBooks.add(enfantTerre02);
		listBooks.add(enfantTerre03);
		listBooks.add(enfantTerre04);
		listBooks.add(enfantTerre05);
		listBooks.add(enfantTerre06);
		listBooks.add(aventuriersMer02);
		listBooks.add(aventuriersMer03);

        return listBooks;
    }

    public static List<BookDTO> booksToBooksDTO(List<Book> listBook) {
		List<BookDTO> booksDTOList = new ArrayList<>();
		listBook.forEach(book -> {
			BookDTO bookDTO = new BookDTO(
					book.getId(),
					book.getTitle(),
					book.getAuthor(),
					book.getVolume(),
					book.getSerie(),
					book.getPicture(),
					"",
					"",
					"",
					"false"
			);
			booksDTOList.add(bookDTO);
		});

		return booksDTOList;
	}

	public static List<BookDTO> booksApiToBooksDTO(List<BookApi> listBook) {
		List<BookDTO> booksDTOList = new ArrayList<>();
		listBook.forEach(book -> {
			BookDTO bookDTO = new BookDTO(
					book.getId(),
					book.getTitle(),
					book.getAuthor(),
					book.getVolume(),
					book.getSerie(),
					book.getPicture(),
					book.getSummary(),
					book.getLanguage(),
					book.getPublisher(),
					"false"
			);
			booksDTOList.add(bookDTO);
		});

		return booksDTOList;
	}
	
}
