package com.example.bookslist.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Data
@ToString
public class BookApi
{

	@NonNull
	private long id;

	@NonNull
	private String title;

	@NonNull
	private String slug;

	@NonNull
	private String author;

	private String summary;

	private String picture;

	private String serie;

	private int volume;

	private String language;

	private String publisher;

	@NonNull
	private Meta meta;

}
