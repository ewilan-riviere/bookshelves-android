package com.example.bookslist.models;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class Book
{

	private int id;
	private String title;
	private String author;
	private String isbn;
	private double price;
	private int volume;
	private String serie;
	private LocalDate date;
	private String picture;

	@BindingAdapter("pictureImage")
	public static void loadImage(ImageView view, String imageUrl) {
		Glide.with(view.getContext())
				.load(imageUrl).apply(new RequestOptions().circleCrop())
				.into(view);
	}
	
}
